export default (await import('vue')).defineComponent({
  name: 'App',
  data: () => ({
    tasks: [
      {
        text: 'Изучить VueJS',
        completed: true,
      },
      {
        text: 'Разработать ToDo на VueJS',
        completed: false,
      },
    ],
  }),
  methods: {
    onToggleCompleted(index) {
      this.tasks[index].completed = !this.tasks[index].completed;
    },
    onRemoveTask(index) {
      this.tasks.splice(index, 1);
    },
    onAddTask(text) {
      this.tasks.push({
        text,
        completed: false,
      });
    },
  },
  components: {
    ListItem,
    TaskField,
  },
});
